#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <Controller.h>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    Contractor::registerTasks();
    Controller TaskController;
    ContractorTask Task;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("TaskController", &TaskController);
    engine.rootContext()->setContextProperty("Task", &Task);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
