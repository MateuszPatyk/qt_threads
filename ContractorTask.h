#ifndef TASK_H
#define TASK_H

#include <QObject>
#include <QVariant>
#include <QThread>
#include <QDebug>

class ContractorTask : public QObject
{
    Q_OBJECT

public:
    explicit ContractorTask(QObject *parent = nullptr) :
        QObject(parent),
        taskID(-1),
        contractorID(-1),
        data("")
    {
        // ...
    }

    Q_INVOKABLE int getTaskID()
    {
        qDebug() << "getTaskID()" << QThread::currentThreadId();
        return taskID;
    }

    Q_INVOKABLE void setTaskID(int _ID)
    {
        qDebug() << "setTaskID()" << QThread::currentThreadId();
        taskID = _ID;
    }

    Q_INVOKABLE int getConctractorID()
    {
        qDebug() << "getConctractorID()" << QThread::currentThreadId();
        return contractorID;
    }

    Q_INVOKABLE void setContractorID(int _ID)
    {
        qDebug() << "setContractorID()" << QThread::currentThreadId();
        contractorID = _ID;
    }

    Q_INVOKABLE QVariant getData()
    {
        qDebug() << "getData()" << QThread::currentThreadId();
        return data;
    }

    Q_INVOKABLE void setData(QVariant _data)
    {
        qDebug() << "setData()" << QThread::currentThreadId();
        data = _data;
    }

private:
    int taskID;
    int contractorID;
    QVariant data;
};

#endif // TASK_H
