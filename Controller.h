#ifndef CONTROLLERv2_H
#define CONTROLLERv2_H

#include <QObject>
#include <QThread>
#include <QDebug>

#include <Contractor.h>
#include <ContractorTask.h>

class Controller : public QObject
{
    Q_OBJECT
    QThread workerThread;

public:
    Controller()
    {
        Contractor *worker = new Contractor(0);
        worker->moveToThread(&workerThread);
        connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        connect(this, &Controller::startTask, worker, &Contractor::executeTask);
        connect(worker, &Contractor::taskFinished, this, &Controller::handleResults);
        workerThread.start();
    }

    ~Controller()
    {
        workerThread.quit();
        workerThread.wait();
    }

signals:
    void startTask(ContractorTask*);

public slots:
    void handleResults(ContractorTask* _task)
    {
        qDebug() << _task->getTaskID() << _task->getConctractorID() << _task->getData();
    }
};

#endif // CONTROLLERv2_H
