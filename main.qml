import QtQuick 2.12
import QtQuick.Controls 2.5

import Tasks 1.0

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Test")

    Button {
        id: button
        text: qsTr("Button")
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        onClicked: {
            var items = [1.25, 2.12345, 2.2, 1.25, 2.12345, 2.2, 1.25, 2.12345, 2.2, 1.25, 2.12345, 2.2]

            Task.setData(items)
            runTaskFromQML(ContractorTasks.GET_QVARIANT_FROM_QML, 0)
            runTaskFromQML(ContractorTasks.GET_QVARIANT_FROM_QML, 0)
            runTaskFromQML(ContractorTasks.GET_QVARIANT_FROM_QML, 0)
            runTaskFromQML(ContractorTasks.GET_QVARIANT_FROM_QML, 0)
        }
    }

    function runTaskFromQML(_taskId, _contractorID)
    {
        console.log("Running worker task from QML, ID = ", _taskId, " Contractor ID = ", _contractorID)

        Task.setTaskID(_taskId)
        Task.setContractorID(_contractorID)
        TaskController.startTask(Task)
    }
}
