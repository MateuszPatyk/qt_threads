#ifndef CONTRACTOR_H
#define CONTRACTOR_H

#include <QObject>
#include <QQmlEngine>
#include <QThread>
#include <QMutex>

#include <ContractorTask.h>

//#define CONTRACTOR_DEBUG_LOCK

class Contractor : public QObject
{
    Q_OBJECT

public:
    Contractor(int _ID, QObject* parent = nullptr);
    ~Contractor();

    enum class Tasks : int
    {
        NOT_DEFINED = -1,
        Q_LIST_VARIANTS_TO_QML,
        GET_QVARIANT_FROM_QML
    };

    Q_ENUMS(Tasks)

    inline static void registerTasks()
    {
        qmlRegisterUncreatableType<Contractor>("Tasks", 1, 0, "ContractorTasks", "ContractorTask is uncreatable in QML");
    }

    int getID()
    {
        return ID;
    }

public slots:
    void executeTask(ContractorTask* _task);

signals:
    void finished();
    void taskStarted(ContractorTask* _task);
    void taskFinished(ContractorTask* _task);

private:
    int ID;

#ifndef CONTRACTOR_DEBUG_LOCK
    Qt::HANDLE mainThreadID;
#endif
};

#endif // CONTRACTOR_H
