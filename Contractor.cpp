#include "Contractor.h"

#include <QDebug>
#include <QThread>

Contractor::Contractor(int _ID, QObject *parent) :
    QObject(parent),
    ID(_ID)
{
#ifndef CONTRACTOR_DEBUG_LOCK
    mainThreadID = QThread::currentThreadId();
#endif
}

Contractor::~Contractor()
{
    // ...

    qDebug() << "Cleaning SerialPortContractor...";
}

void Contractor::executeTask(ContractorTask* _task)
{
    emit(taskStarted(_task));

    qDebug() << "\tCONTRACTOR STARTED";

    int localContractorID = _task->getConctractorID();

    if(localContractorID != getID())
    {
        qDebug() << "Not mine ID: " << localContractorID << "discarding";
        return;
    }

    qDebug() << "Emiting signal from Contractor";
    emit(taskStarted(_task));

    int localTaskID = _task->getTaskID();
    QVariant localData = _task->getData();

    QList<QVariant> _params;
    QList<float> _floats;
    _params = localData.value<QList<QVariant>>();

    for(auto item : _params)
    {
        _floats << item.toFloat();
    }

#ifndef CONTRACTOR_DEBUG_LOCK
    qDebug() << "Main ThreadID =" << mainThreadID << "my ThreadID =" << QThread::currentThreadId();
#endif

    switch(static_cast<Tasks>(localTaskID))
    {
    case Tasks::Q_LIST_VARIANTS_TO_QML :
    {
        QList<QVariant> _params;
        _params.append(12.5F);
        _params.append(14.36F);

        QVariant _data = _params;

        _task->setData(_data);
    }
        break;
    case Tasks::GET_QVARIANT_FROM_QML:
    {
        int i = 0;
        for(auto item : _floats)
        {
            qDebug() << "C++ item" << i++ << "value" << item;
        }
    }
        break;
    default:
    {
#ifndef CONTRACTOR_DEBUG_LOCK
        qDebug() << "Oh... I don't have these one :(";
#endif
    }
    }

    emit(taskFinished(_task));

    qDebug() << "\tCONTRACTOR ENDED";
}
